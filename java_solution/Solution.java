package java_solution;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Solution {
    static String inputFilesDirectory = "./input/";
    static String outputFilesDirectory = "java_solution/output/";

    public static void main(String[] args) throws Exception {

        String[] fileNames = { "a", "b", "c", "d", "e", "f" };

        long start_time = System.currentTimeMillis();
        System.out.println("Start : " + (System.currentTimeMillis() - start_time) + " ms, soit "
                + (System.currentTimeMillis() - start_time) / 60000 + " min");

        for (String fileName : fileNames) {
            read(fileName);
            process(fileName);
            System.out.println("Time process " + fileName + ": " + (System.currentTimeMillis() - start_time)
                    + " ms, soit " + (System.currentTimeMillis() - start_time) / 60000 + " min");
        }

        System.out.println("Time before score calculation: " + (System.currentTimeMillis() - start_time) + " ms, soit "
                + (System.currentTimeMillis() - start_time) / 60000 + " min");

        getScores(fileNames);
        System.out.println("Time after score calculation: " + (System.currentTimeMillis() - start_time) + " ms, soit "
                + (System.currentTimeMillis() - start_time) / 60000 + " min");

    }

    private static void read(String fileName) throws IOException {
        // Lecture du fichier input
        BufferedReader reader = new BufferedReader(new FileReader(inputFilesDirectory + fileName + ".in"));

        // La première ligne
        String firstLine = reader.readLine();
        String[] vars = firstLine.split(" ");

        // le reste des données (ligne)
        String line;
        while ((line = reader.readLine()) != null) {
            // données de la ligne
            String values[] = line.split(" ");
        }

        reader.close();
    }

    private static void process(String fileName) throws IOException {
        // ecriture fichier output
        FileWriter writer = new FileWriter(outputFilesDirectory + fileName + ".out");
        writer.write("0" + "\n");
    }

    private static void getScores(String[] fileNames) throws IOException {
        int scoreTotal = 0;
        for (String fileName : fileNames) {
            int score = 0;

            BufferedReader reader = new BufferedReader(new FileReader(outputFilesDirectory + fileName + ".out"));
            reader.readLine();

            String line;
            while ((line = reader.readLine()) != null) {
                String values[] = line.split(" ");
                for (int i = 1; i < values.length; i++) {
                }
            }

            reader.close();
            scoreTotal += score;
            System.out.println("SCORE FOR " + fileName + ": " + score);
        }
        System.out.println("SCORE TOTAL : " + scoreTotal);
    }
}
