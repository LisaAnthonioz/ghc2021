import time
import datetime

inputFilesDirectory = "input/"
outputFilesDirectory = "python/output/"

fileNames = ["a", "b", "c", "d", "e", "f"]


def read(fileName):
    # lecture du fichier input
    inputFile = open(inputFilesDirectory + fileName + ".in", "rt")
    lines = inputFile.read().splitlines()

    # la première ligne
    firstLine = lines[0].strip()

    # le reste des données (ligne)
    for line in lines[1:]:
        values = line.split(" ")
        # données de la ligne
        # for i in range(1, len(values)):

    inputFile.close()


def process(fileName):
    # ecriture fichier output
    outputFile = open(outputFilesDirectory + fileName + ".out", "w")
    outputFile.write("0" + "\n")
    outputFile.close()


def getScores(fileNames):
    scoreTotal = 0
    for fileName in fileNames:
        score = 0

        outputFile = open(outputFilesDirectory + fileName + ".out", "rt")
        lines = outputFile.read().splitlines()
        for line in lines[1:]:
            values = line.split(" ")

        outputFile.close()
        scoreTotal += score
        print("SCORE FOR ", fileName, ":", str(score))

    print("SCORE TOTAL : ", str(scoreTotal))


start_time = time.time() * 1000
print("Start : ", datetime.datetime.now().hour, ":",
      datetime.datetime.now().minute, ":", datetime.datetime.now().second)
for fileName in fileNames:
    read(fileName)
    process(fileName)
    print("Time process ", fileName, ":", (time.time()*1000 - start_time) /
          60000, "ms, soit ", (time.time()*1000 - start_time)/60000, " min")

print("Time before score calculation ", fileName, ":", (time.time()*1000 - start_time) /
      60000, "ms, soit ", (time.time()*1000 - start_time)/60000, " min")
getScores(fileNames)
print("Time after score calculation ", fileName, ":", (time.time()*1000 - start_time) /
      60000, "ms, soit ", (time.time()*1000 - start_time)/60000, " min")


print("End : ", datetime.datetime.now().hour, ":",
      datetime.datetime.now().minute, ":", datetime.datetime.now().second)
