import { readFileSync, writeFileSync } from "fs";

class Street {
  start: number;
  end: number;
  street: string;
  time: number;

  constructor(start: number, end: number, street: string, time: number) {
    this.start = start;
    this.end = end;
    this.street = street;
    this.time = time;
  }
}
class Car {
  nbStreets: number;
  streets: string[];
  constructor(nbStreets: number, streets: string[]) {
    this.nbStreets = nbStreets;
    this.streets = streets;
  }
}

class Main {
  inputFilesDirectory = "input/";
  outputFilesDirectory = "ts/output/";

  fileNames = ["c"];
  tempsTotal = 0;
  nbIntersections = 0;
  nbStreets = 0;
  nbCars = 0;
  ptBonus = 0;

  mapStreet = new Map();
  listStreets: string[] = [];

  main() {
    let start_time = new Date().getTime();
    console.log(
      "Start : ",
      new Date().getHours() +
      ":" +
      new Date().getMinutes() +
      ":" +
      new Date().getSeconds()
    );
    for (let fileName of this.fileNames) {
      this.mapStreet = new Map();
      this.read(fileName);
      this.process(fileName);
      console.log(
        "Time process " + fileName + ": ",
        new Date().getTime() - start_time,
        "ms, soit " + (new Date().getTime() - start_time) / 60000 + " min"
      );
    }
    console.log(
      "Time before score calculation: ",
      new Date().getTime() - start_time,
      "ms, soit " + (new Date().getTime() - start_time) / 60000 + " min"
    );
    this.getScores(this.fileNames);
    console.log(
      "Time after score calculation: ",
      new Date().getTime() - start_time,
      "ms, soit " + (new Date().getTime() - start_time) / 60000 + " min"
    );

    console.log(
      "End : ",
      new Date().getHours() +
      ":" +
      new Date().getMinutes() +
      ":" +
      new Date().getSeconds()
    );
  }

  read(fileName) {
    this.mapStreet = new Map(); // rue / intersection
    this.listStreets = [];
    // Lecture du fichier input
    let data = readFileSync(
      this.inputFilesDirectory + fileName + ".in"
    ).toString("utf-8");
    let lines = data.split("\n").slice(0, -1);

    // la première ligne
    let firstLine = lines[0].trim().split(" ");
    this.tempsTotal = +firstLine[0];
    this.nbIntersections = +firstLine[1];
    this.nbStreets = +firstLine[2];
    this.nbCars = +firstLine[3];
    this.ptBonus = +firstLine[4];

    // voitures
    for (let i = this.nbStreets + 1; i < this.nbStreets + this.nbCars + 1; i++) {
      let values = lines[i].split(" ");
      this.listStreets = this.listStreets.concat(values.slice(1));
    }

    // rues
    for (let i = 1; i < this.nbStreets + 1; i++) {
      let values = lines[i].split(" ");
      let map = this.mapStreet.get(values[1]) ? this.mapStreet.get(values[1]) : new Map();
      // this.listStreets.push(new Street(+values[0], +values[1], values[2], +values[3]));
      // inter / rue / poids
      if (this.listStreets.indexOf(values[2]) > -1) {
        this.mapStreet.set(values[1], map.set(values[2], 0));
      }
    }

    this.listStreets.forEach(s => {
      this.mapStreet.forEach(mS => {
        if (mS.has(s)) {
          mS.set(s, mS.get(s) + 1);
        }
      });
    })

    this.listStreets.forEach((s) => {
      this.mapStreet.forEach((value: Map<string, number>, key: string) => {
        let somme = 0;
        if (this.mapStreet.get(key).has(s)) {
          value.forEach((v: number, k: string) => {
            somme += v;
          });
          let c = this.convertPoidsToSec(this.mapStreet.get(key).get(s), somme);
          this.mapStreet.get(key).set(s, c);
        }
      });
    });
  }

  convertPoidsToSec(poids: number, somme: number) {
    let cycle = this.tempsTotal / 50;
    // let round = Math.ceil(poids / this.nbCars / 10);
    let round = Math.ceil((poids * cycle) / somme);
    return round > this.tempsTotal ? this.tempsTotal : round < 1 ? 1 : round;
  }

  process(fileName) {
    // ecriture fichier ouput
    let outputFileName = this.outputFilesDirectory + fileName + ".out";
    let data = this.mapStreet.size.toString() + "\n";
    this.mapStreet.forEach((value: Map<string, number>, key: string) => {
      data += key + "\n";
      data += value.size + "\n";
      value.forEach((v: number, k: string) => {
        data += k + " " + v + "\n";
      });
    });
    writeFileSync(outputFileName, data);
  }

  getScores(fileNames) {
    let scoreTotal = 0;
    for (let fileName of fileNames) {
      let score = 0;

      let data = readFileSync(
        this.outputFilesDirectory + fileName + ".out"
      ).toString("utf-8");

      let lines = data.split("\n").slice(0, -1);
      for (let line of lines.slice(1)) {
        let values = line.split(" ");

        for (let val of values.slice(1)) {
        }
      }

      scoreTotal += score;
      console.log("SCORE FOR " + fileName + ": " + score);
    }
    console.log("SCORE TOTAL : " + scoreTotal);
  }
}

let main = new Main();
main.main();